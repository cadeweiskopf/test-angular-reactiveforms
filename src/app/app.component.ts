import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'test';

  firstName = new FormControl<string>('', Validators.required);
  lastName = new FormControl<string>('', Validators.required);
  test = new FormControl<string>('', Validators.required);

  appForm = this.formBuilder.group({
    firstName: this.firstName,
  });

  appForm2 = this.formBuilder.group({
    lastName: this.lastName,
    test: this.test,
  });

  constructor(private formBuilder: FormBuilder) { }
  
  ngOnInit(): void {
    const abstractForm = {
      
    }
  }
}
