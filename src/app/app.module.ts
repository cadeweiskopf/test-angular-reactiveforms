import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { FormBuilder } from '@angular/forms';

import { AppComponent } from './app.component';
import { CustomComponentComponent } from './custom-component/custom-component.component';

@NgModule({
  declarations: [AppComponent, CustomComponentComponent],
  imports: [BrowserModule, ReactiveFormsModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {

}
